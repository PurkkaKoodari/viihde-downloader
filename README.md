# Viihde Downloader

Viihde Downloader is an application that can be used to save recordings
from Elisa Viihde locally. This can be used to keep recordings longer
than the two years allowed on the platform.

Viihde Downloader is written in Java 8/JavaFX and licensed under MIT. It
uses the [org.json](https://mvnrepository.com/artifact/org.json/json)
library available from Maven and licensed under the JSON License.

The repository contains an IntelliJ IDEA project, and thus IDEA should
be used to develop it.

## Features

  - Folder tree view of recordings
  - Filtering of recordings by name
  - Multiple simultaneous downloads (currently limited to 3)
  - Entire folder tree downloads with directory structure
  - Download queue
  - Automatic resuming of failed downloads
