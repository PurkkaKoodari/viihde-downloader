package net.pietu1998.viihdedownloader;

import java.net.URL;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Represents a request to download an URL.
 *
 * @param <R> the desired return type of the download
 */
public class LoadJob<R> implements Comparable<LoadJob<?>> {

    /**
     * The URL to download.
     */
    private final URL url;
    /**
     * A function that takes the downloaded data as a string and returns the desired result.
     */
    private final Function<String, R> resultConverter;
    /**
     * A callback to call when the download completes successfully.
     */
    private Consumer<R> callback;
    /**
     * A callback to call when the download fails.
     */
    private Consumer<Throwable> errorCallback;
    /**
     * The sequence number of this job. Used for sorting jobs in creation order.
     */
    private final long sequenceNo;

    /**
     * The rolling sequence number for jobs.
     */
    private static final AtomicLong SEQUENCE = new AtomicLong(0);

    /**
     * Creates a new job.
     *
     * @param url             the URL to download
     * @param resultConverter a function that takes the downloaded data as a string and returns the desired result
     */
    public LoadJob(URL url, Function<String, R> resultConverter) {
        this.url = Objects.requireNonNull(url, "url");
        this.resultConverter = Objects.requireNonNull(resultConverter, "resultConverter");
        this.sequenceNo = SEQUENCE.incrementAndGet();
    }

    /**
     * Gets the URL to download.
     *
     * @return the URL
     */
    public URL getUrl() {
        return url;
    }

    /**
     * Sets the callback to call when the download completes successfully. The callback is called with the result as
     * the argument.
     *
     * @param callback the callback
     */
    public void onSuccess(Consumer<R> callback) {
        this.callback = callback;
    }

    /**
     * Sets the callback to call when the download fails. The callback is called with the causing exception as the
     * argument.
     *
     * @param errorCallback the callback
     */
    public void onError(Consumer<Throwable> errorCallback) {
        this.errorCallback = errorCallback;
    }

    /**
     * Called by the loader thread when the download completes successfully. Calls the callback, if any.
     *
     * @param result the downloaded data as a string
     */
    public void success(String result) {
        if (callback != null)
            callback.accept(resultConverter.apply(result));
    }

    /**
     * Called by the loader thread when the download fails. Calls the error callback, if any.
     *
     * @param error the exception that caused the download to fail
     */
    public void error(Throwable error) {
        if (errorCallback != null)
            errorCallback.accept(Objects.requireNonNull(error));
    }

    /**
     * Compares two jobs. A {@link LoginLoadJob} compares as less than any other type of job; a {@link RecordingLoadJob}
     * compares as greater than any other type of job. Finally, a job compares as less than another created after it.
     *
     * @param o the job to compare to
     * @return a negative integer, zero, or a positive integer as this job is less than, equal to, or greater than
     * the specified job
     */
    @Override
    public int compareTo(LoadJob<?> o) {
        if (this instanceof LoginLoadJob) {
            if (!(o instanceof LoginLoadJob))
                return -1;
        } else if (o instanceof LoginLoadJob) {
            return 1;
        } else if (this instanceof RecordingLoadJob) {
            if (!(o instanceof RecordingLoadJob))
                return 1;
        } else if (o instanceof RecordingLoadJob) {
            return -1;
        }
        return Long.compare(sequenceNo, o.sequenceNo);
    }
}
