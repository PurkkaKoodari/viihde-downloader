package net.pietu1998.viihdedownloader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Represents a folder in the folder tree.
 */
public class Folder extends FolderTreeItem {

    /**
     * This folder's subfolders.
     */
    private final List<Folder> subfolders = new ArrayList<>();
    /**
     * This folder's recordings.
     */
    private final List<Recording> recordings = new ArrayList<>();
    /**
     * The number of matching recordings in this folder, not including subfolders. Used for calculating total
     * download counts.
     */
    private int recordingCount;
    /**
     * The number of matching recordings in this folder and subfolders. Only shown in the folder tree.
     */
    private int matchedRecordingCount;

    /**
     * Creates a new folder.
     *
     * @param parent the parent of this folder, or {@code null} if root folder
     * @param id     the ID of this folder, or {@code ""} if root folder
     * @param name   the name of this folder
     */
    public Folder(Folder parent, String id, String name) {
        super(parent, id, name);
        if (parent != null)
            parent.subfolders.add(this);
    }

    /**
     * Sets the count of matching recordings in this folder, not including subfolders. This is used for calculating
     * total download counts.
     *
     * @param recordingCount the new count
     */
    public void setRecordingCount(int recordingCount) {
        this.recordingCount = recordingCount;
    }

    /**
     * Sets the total count of matching recordings in this folder and subfolders. This is only shown in the folder tree.
     *
     * @param matchedRecordingCount the new count
     */
    public void setMatchedRecordingCount(int matchedRecordingCount) {
        this.matchedRecordingCount = matchedRecordingCount;
    }

    /**
     * Adds a recording to this folder.
     *
     * @param recording the recording to add
     */
    public void addRecording(Recording recording) {
        recordings.add(Objects.requireNonNull(recording));
    }

    /**
     * Gets an unmodifiable list of this folder's subfolders.
     *
     * @return the list of subfolders
     */
    public List<Folder> getSubfolders() {
        return Collections.unmodifiableList(subfolders);
    }

    /**
     * Gets an unmodifiable list of this folder's recordings, not including subfolders.
     *
     * @return the list of recordings
     */
    public List<Recording> getRecordings() {
        return Collections.unmodifiableList(recordings);
    }

    @Override
    public Stream<Recording> recordings() {
        return Stream.concat(recordings.stream(), subfolders.stream().map(Folder::recordings).reduce(Stream.empty(), Stream::concat));
    }

    @Override
    public int getRecordingCount() {
        return subfolders.stream().mapToInt(Folder::getRecordingCount).sum() + recordingCount;
    }

    @Override
    public String toString() {
        return getName() + " (" + matchedRecordingCount + ")";
    }

}
