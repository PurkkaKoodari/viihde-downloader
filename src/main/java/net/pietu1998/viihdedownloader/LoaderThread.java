package net.pietu1998.viihdedownloader;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class LoaderThread extends Thread {

    /**
     * The maximum number of retries for a request.
     */
    private static final int MAX_RETRIES = 3;
    /**
     * The minimum time to wait between progress updates.
     */
    private static final int SPEED_UPDATE_DELAY = 500;

    /**
     * The rolling ID of the thread, used for the thread name.
     */
    private static int id = 1;

    /**
     * Whether or not this thread is reserved for API calls and can't process recording downloads.
     */
    private final boolean reserved;
    /**
     * The shared map of cookies.
     */
    private final ConcurrentMap<String, String> cookies;

    /**
     * The job being currently processed.
     */
    private LoadJob<?> currentJob;
    /**
     * Whether or not this thread has been asked to stop.
     */
    private volatile boolean quitting = false;

    /**
     * The queue to take jobs from.
     */
    private final JobQueue jobQueue;
    /**
     * The lock for {@link #noJobs}.
     */
    private final ReentrantLock jobLock;
    /**
     * The condition to wait on if no jobs are available.
     */
    private final Condition noJobs;

    /**
     * Creates a new loader thread.
     *
     * @param reserved whether or not this thread is reserved for API calls and can't process recording downloads
     * @param cookies  the shared map of cookies
     * @param jobQueue the queue to take jobs from
     * @param jobLock  the lock for {@code noJobs}
     * @param noJobs   the condition to wait on if no jobs are available
     */
    public LoaderThread(boolean reserved, ConcurrentMap<String, String> cookies, JobQueue jobQueue, ReentrantLock jobLock, Condition noJobs) {
        super("Loader thread " + id++);
        this.reserved = reserved;
        this.cookies = cookies;
        this.jobQueue = jobQueue;
        this.jobLock = jobLock;
        this.noJobs = noJobs;
    }

    @Override
    public void run() {
        while (!quitting) {
            jobLock.lock();
            while (!quitting) {
                LoadJob<?> availableJob = jobQueue.peek();
                if (!(availableJob instanceof RecordingLoadJob) || !reserved) {
                    currentJob = jobQueue.poll();
                    if (currentJob != null)
                        break;
                }
                try {
                    noJobs.awaitNanos(100_000_000);
                } catch (InterruptedException ignore) {}
            }
            jobLock.unlock();
            if (quitting)
                return;
            if (currentJob instanceof RecordingLoadJob)
                executeRecordingJob();
            else
                executeSimpleJob();
        }
        currentJob = null;
    }

    /**
     * Downloads an URL to the specified path, with resume support.
     */
    private void executeRecordingJob() {
        RecordingLoadJob job = (RecordingLoadJob) currentJob;
        long totalBytes = -1, previouslyLoaded, bytesLoaded = 0;
        boolean restarting = false;
        // Open the output file
        OpenOption options[];
        if (job.isAllowOverwrite())
            options = new OpenOption[]{StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING};
        else
            options = new OpenOption[]{StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW};
        try (FileChannel channel = FileChannel.open(job.getDownloadPath(), options)) {
            for (int retry = 1; !quitting && retry <= MAX_RETRIES; retry++) {
                previouslyLoaded = bytesLoaded;
                try {
                    URL url = job.getUrl();
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestProperty("Cookie", makeCookieString());
                    connection.connect();
                    // Reload the recording's details if unauthorized
                    if (connection.getResponseCode() == 401) {
                        restarting = true;
                        break;
                    }
                    Range range = parseRangeAndCookies(connection);
                    // Update the position and length
                    if (range != null) {
                        if (totalBytes == -1) {
                            totalBytes = range.length;
                            job.updateLength(totalBytes);
                        }
                        previouslyLoaded = range.start;
                    }
                    try (InputStream stream = connection.getInputStream()) {
                        ReadableByteChannel inputChannel = Channels.newChannel(stream);
                        ByteBuffer buffer = ByteBuffer.allocate(8192);
                        channel.position(previouslyLoaded);
                        long speedAmount = 0;
                        long previousTime = System.currentTimeMillis();
                        while (!quitting && !job.isCancelRequested() && inputChannel.read(buffer) != -1) {
                            buffer.flip();
                            channel.write(buffer);
                            bytesLoaded += buffer.limit();
                            // Calculate the loading speed
                            speedAmount += buffer.limit();
                            long currentTime = System.currentTimeMillis();
                            long speedTime = currentTime - previousTime;
                            if (speedTime >= SPEED_UPDATE_DELAY) {
                                float speed = 1000f * speedAmount / speedTime;
                                job.updateProgress(bytesLoaded, speed);
                                speedAmount = 0;
                                previousTime = currentTime;
                            }
                            buffer.clear();
                        }
                    }
                    break;
                } catch (IOException e) {
                    // Don't count a retry if we got any data
                    if (totalBytes != -1 && bytesLoaded > previouslyLoaded)
                        retry--;
                    if (retry == MAX_RETRIES) {
                        job.error(e);
                        return;
                    }
                }
            }
        } catch (IOException e) {
            job.error(e);
            return;
        }
        if (quitting)
            return;
        // Delete the file if it's no longer needed
        if ((restarting && !job.isAllowOverwrite()) || job.isCancelRequested()) {
            try {
                Files.deleteIfExists(job.getDownloadPath());
            } catch (IOException ignore) {}
        }
        // Update the job status
        if (job.isCancelRequested())
            job.canceled();
        else if (restarting)
            job.restarted();
        else
            job.success(null);
    }

    /**
     * Downloads an URL into memory, without resume support.
     */
    private void executeSimpleJob() {
        for (int retry = 1; retry <= MAX_RETRIES; retry++) {
            try {
                URL url = currentJob.getUrl();
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Cookie", makeCookieString());
                connection.connect();
                parseRangeAndCookies(connection);
                StringBuilder resultBuilder = new StringBuilder();
                try (Reader reader = new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8"))) {
                    char buffer[] = new char[1024];
                    int read;
                    while (!quitting && (read = reader.read(buffer)) != -1)
                        resultBuilder.append(new String(buffer, 0, read));
                }
                if (!quitting)
                    currentJob.success(resultBuilder.toString());
                break;
            } catch (IOException | JSONException e) {
                if (retry == MAX_RETRIES)
                    currentJob.error(e);
            }
        }
    }

    /**
     * Parses the response headers for any cookies and result ranges. Saves cookies to the cookie map and returns any
     * range.
     *
     * @param connection the connection to get the headers from
     * @return the response range, if any
     */
    private Range parseRangeAndCookies(HttpURLConnection connection) {
        Range range = null;
        for (int i = 1; ; i++) {
            String headerName = connection.getHeaderFieldKey(i);
            if (headerName == null)
                break;
            String headerValue = connection.getHeaderField(i);
            if (headerName.equalsIgnoreCase("Set-Cookie")) {
                int index = headerValue.indexOf(";");
                String valuePart = index == -1 ? headerValue : headerValue.substring(0, index);
                index = valuePart.indexOf("=");
                if (index == -1)
                    continue;
                String name = valuePart.substring(0, index).trim();
                String value = valuePart.substring(index + 1).trim();
                cookies.put(name, value);
            }
            if (headerName.equalsIgnoreCase("Content-Length") && range == null) {
                try {
                    long length = Long.parseLong(headerValue);
                    range = new Range(0, length);
                } catch (NumberFormatException ignore) {}
            }
            if (headerName.equalsIgnoreCase("Content-Range") && headerValue.startsWith("bytes ")) {
                try {
                    int slashIndex = headerValue.indexOf("/");
                    int dashIndex = headerValue.indexOf("-");
                    long start = Long.parseLong(headerValue.substring(0, dashIndex));
                    long length = Long.parseLong(headerValue.substring(slashIndex + 1));
                    range = new Range(start, length);
                } catch (NumberFormatException | IndexOutOfBoundsException ignore) {}
            }
        }
        return range;
    }

    /**
     * Represents a HTTP Range.
     */
    private class Range {
        final long start;
        final long length;

        private Range(long start, long length) {
            this.start = start;
            this.length = length;
        }
    }

    /**
     * Generates the value for the Cookie header from the cookie map.
     *
     * @return the Cookie header value
     */
    private String makeCookieString() {
        return cookies.entrySet().stream().map(cookie -> cookie.getKey() + "=" + cookie.getValue()).collect(Collectors.joining("; "));
    }

    /**
     * Requests this thread to stop.
     */
    public void quit() {
        quitting = true;
    }

}
