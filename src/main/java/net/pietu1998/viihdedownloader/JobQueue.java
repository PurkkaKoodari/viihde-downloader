package net.pietu1998.viihdedownloader;

import java.util.PriorityQueue;

/**
 * Queues jobs by priority and allows for pausing of non-login jobs.
 */
public class JobQueue {

    /**
     * The queue containing the actual data.
     */
    private final PriorityQueue<LoadJob<?>> data = new PriorityQueue<>();
    /**
     * Whether or not only login jobs should be allowed through.
     */
    private volatile boolean paused = true;

    /**
     * Sets the paused state of this queue. If {@code true}, only login jobs will be returned by {@link #peek()} and
     * {@link #poll()}.
     *
     * @param paused the new paused state
     */
    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    /**
     * Gets the next job from the queue without removing it.
     *
     * @return the next job
     */
    public LoadJob<?> peek() {
        synchronized (data) {
            LoadJob<?> job = data.peek();
            return !paused || job instanceof LoginLoadJob ? job : null;
        }
    }

    /**
     * Gets the next job from the queue and removes it.
     *
     * @return the next job
     */
    public LoadJob<?> poll() {
        synchronized (data) {
            return !paused || data.peek() instanceof LoginLoadJob ? data.poll() : null;
        }
    }

    /**
     * Adds a job to the queue.
     *
     * @param job the job to add
     */
    public void add(LoadJob<?> job) {
        synchronized (data) {
            data.add(job);
        }
    }

    /**
     * Cancels a job from the queue.
     *
     * @param job the job to cancel
     * @return whether or not the job was canceled
     */
    public boolean remove(RecordingLoadJob job) {
        synchronized (data) {
            return data.remove(job);
        }
    }
}
