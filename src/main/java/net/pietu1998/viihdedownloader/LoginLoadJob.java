package net.pietu1998.viihdedownloader;

import java.net.URL;

/**
 * Represents a job that performs a login.
 */
public class LoginLoadJob extends LoadJob<Boolean> {

    /**
     * Creates a new login job.
     *
     * @param url the URL of the login call
     */
    public LoginLoadJob(URL url) {
        super(url, Boolean::parseBoolean);
    }

}
