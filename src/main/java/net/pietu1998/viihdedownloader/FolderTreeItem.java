package net.pietu1998.viihdedownloader;

import java.util.*;
import java.util.stream.Stream;

/**
 * Represents an item in the folder tree, i.e. a folder or a recording.
 */
public abstract class FolderTreeItem {

    /**
     * The folder containing this item, or {@code null} if this is the root folder.
     */
    private final Folder parent;
    /**
     * The name of this item.
     */
    private final String name;
    /**
     * The ID of this item.
     */
    private final String id;

    protected FolderTreeItem(Folder parent, String id, String name) {
        this.parent = parent;
        this.id = Objects.requireNonNull(id, "id");
        this.name = Objects.requireNonNull(name, "name");
    }

    /**
     * Gets the folder containing this item, or {@code null} if this is the root folder.
     *
     * @return the parent folder
     */
    public Folder getParent() {
        return parent;
    }

    /**
     * Gets the ID of this item.
     *
     * @return the ID
     */
    public final String getId() {
        return id;
    }

    /**
     * Gets the name of this item.
     *
     * @return the name
     */
    public final String getName() {
        return name;
    }

    /**
     * Gets the set of all folders containing this item recursively.
     *
     * @return the set of ancestor folders
     */
    public Set<Folder> ancestorSet() {
        Set<Folder> parents = new HashSet<>();
        for (FolderTreeItem current = this; current.parent != null; current = current.parent)
            parents.add(current.parent);
        return parents;
    }

    /**
     * Gets the list of all folders containing this item recursively, i.e. the path to this item without the item
     * itself. The list is ordered from the root towards the item.
     *
     * @return the list of ancestor folders
     */
    public List<Folder> getAncestors() {
        List<Folder> parents = new ArrayList<>();
        for (FolderTreeItem current = this; current.parent != null; current = current.parent)
            parents.add(current.parent);
        Collections.reverse(parents);
        return parents;
    }

    /**
     * Gets the total count of recordings contained by this item and possible subfolders.
     *
     * @return the number of recordings
     */
    public abstract int getRecordingCount();

    /**
     * Gets a stream of the recordings contained by this item and possible subfolders.
     *
     * @return the stream of recordings
     */
    public abstract Stream<Recording> recordings();

    @Override
    public int hashCode() {
        return getClass().hashCode() ^ id.hashCode();
    }

    /**
     * Checks if this item is equal to the given object. Two {@code FolderTreeItem}s are considered equal if they are
     * of the same class and have the same ID.
     *
     * @param o the object to compare against
     */
    @Override
    public boolean equals(Object o) {
        return o != null && getClass() == o.getClass() && id.equals(((FolderTreeItem) o).id);
    }
}
