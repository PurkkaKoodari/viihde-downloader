package net.pietu1998.viihdedownloader;

import java.net.URL;
import java.nio.file.Path;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Represents a request to download a recording.
 */
public class RecordingLoadJob extends LoadJob<Void> {

    /**
     * The path the recording is being downloaded to.
     */
    private final Path downloadPath;
    /**
     * Whether or not overwriting the target file is allowed.
     */
    private final boolean allowOverwrite;
    /**
     * A callback to call when the total size of the download is known.
     */
    private Consumer<Long> lengthCallback;
    /**
     * A callback to call when the download progress changes.
     */
    private BiConsumer<Long, Float> progressCallback;
    /**
     * A callback to call when the download is canceled successfully.
     */
    private Runnable cancelCallback;
    /**
     * A callback to call when the download should be restarted.
     */
    private Runnable restartCallback;
    /**
     * Whether or not this job has been requested to cancel.
     */
    private boolean cancelRequested = false;

    /**
     * Creates a new job.
     *
     * @param url            the URL to download
     * @param downloadPath   the path to download to
     * @param allowOverwrite whether or not to allow overwriting the target file
     */
    public RecordingLoadJob(URL url, Path downloadPath, boolean allowOverwrite) {
        super(url, result -> null);
        this.downloadPath = Objects.requireNonNull(downloadPath);
        this.allowOverwrite = allowOverwrite;
    }

    /**
     * Gets the path being downloaded to.
     *
     * @return the path
     */
    public Path getDownloadPath() {
        return downloadPath;
    }

    /**
     * Gets whether or not overwriting the target file is allowed.
     *
     * @return whether or not overwriting is allowed
     */
    public boolean isAllowOverwrite() {
        return allowOverwrite;
    }

    /**
     * Sets the callback to call when the total size of the download becomes known. The callback is called with the
     * size as the argument.
     *
     * @param lengthCallback the callback
     */
    public void onLengthUpdate(Consumer<Long> lengthCallback) {
        this.lengthCallback = lengthCallback;
    }

    /**
     * Sets the callback to call when the download progress updates. The callback is called with the number of
     * downloaded bytes and the number of bytes downloaded per second as arguments.
     *
     * @param progressCallback the callback
     */
    public void onProgress(BiConsumer<Long, Float> progressCallback) {
        this.progressCallback = progressCallback;
    }

    /**
     * Sets the callback to call when the download is canceled successfully.
     *
     * @param cancelCallback the callback
     */
    public void onCancel(Runnable cancelCallback) {
        this.cancelCallback = cancelCallback;
    }

    /**
     * Sets the callback to call when the download should be restarted.
     *
     * @param restartCallback the callback
     */
    public void onRestart(Runnable restartCallback) {
        this.restartCallback = restartCallback;
    }

    /**
     * Gets whether or not this download has been requested to cancel.
     *
     * @return whether or not to cancel the download
     */
    public boolean isCancelRequested() {
        return cancelRequested;
    }

    /**
     * Requests the loader thread to cancel this download.
     */
    public void requestCancel() {
        cancelRequested = true;
    }

    /**
     * Called by the loader thread when the total size of the download becomes known. Calls the length callback, if any.
     *
     * @param length the total size of the download in bytes
     */
    public void updateLength(long length) {
        if (lengthCallback != null)
            lengthCallback.accept(length);
    }

    /**
     * Called by the loader thread when the download progress is updated. Calls the progress callback, if any.
     *
     * @param progress the number of bytes downloaded
     * @param speed    the download speed as bytes per second
     */
    public void updateProgress(long progress, float speed) {
        if (progressCallback != null)
            progressCallback.accept(progress, speed);
    }

    /**
     * Called by the loader thread when the download is successfully canceled. Calls the cancel callback, if any.
     */
    public void canceled() {
        if (cancelCallback != null)
            cancelCallback.run();
    }

    /**
     * Called by the loader thread when the download should be restarted. Calls the restart callback, if any.
     */
    public void restarted() {
        if (restartCallback != null)
            restartCallback.run();
    }
}
