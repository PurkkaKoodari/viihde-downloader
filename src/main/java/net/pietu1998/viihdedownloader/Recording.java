package net.pietu1998.viihdedownloader;

import java.util.Calendar;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Represents a recording in the folder tree.
 */
public class Recording extends FolderTreeItem {

    /**
     * The description of this recording.
     */
    private String description;
    /**
     * The textual duration of this recording. May not be parseable.
     */
    private String duration;
    /**
     * The URL used to download this recording, or {@code null} if the details are not loaded.
     */
    private String downloadURL;
    /**
     * The date of this recording.
     */
    private final Calendar date;

    /**
     * Creates a new recording.
     *
     * @param parent the folder containing this recording
     * @param id     the ID of this recording
     * @param name   the name of this recording
     * @param date   the date of this recording
     */
    public Recording(Folder parent, String id, String name, Calendar date) {
        super(parent, id, name);
        resetDetails();
        Objects.requireNonNull(parent).addRecording(this);
        this.date = Objects.requireNonNull(date);
    }

    /**
     * Gets the date of this recording.
     *
     * @return the date of this recording
     */
    public Calendar getDate() {
        return date;
    }

    /**
     * Gets the description of this recording.
     *
     * @return the description of this recording
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the textual duration of this recording. May not be parseable.
     *
     * @return the duration of this recording
     */
    public String getDuration() {
        return duration;
    }

    /**
     * Gets the download URL of this recording, or {@code null} if the details are not loaded.
     *
     * @return the download URL
     */
    public String getDownloadURL() {
        return downloadURL;
    }

    /**
     * Gets whether or not this recording's details are loaded, i.e. if {@link #setDetails(String, String, String)} has
     * been called with a non-null URL after creation or any calls to {@link #resetDetails()}.
     *
     * @return whether or not the details are loaded
     */
    public boolean isLoaded() {
        return downloadURL != null;
    }

    /**
     * Sets this recording's details.
     *
     * @param description the new description
     * @param duration    the new textual duration
     * @param downloadURL the new download URL
     */
    public void setDetails(String description, String duration, String downloadURL) {
        this.description = Objects.requireNonNull(description, "description");
        this.duration = Objects.requireNonNull(duration, "duration");
        this.downloadURL = downloadURL;
    }

    /**
     * Resets this recording's details to the unloaded state.
     */
    public void resetDetails() {
        this.setDetails("Loading details...", "", null);
    }

    @Override
    public int getRecordingCount() {
        return 1;
    }

    @Override
    public Stream<Recording> recordings() {
        return Stream.of(this);
    }

    @Override
    public String toString() {
        return getName() + " " + ViihdeDownloader.DATE_FORMAT.format(getDate().getTime());
    }
}
