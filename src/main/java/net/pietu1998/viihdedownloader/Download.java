package net.pietu1998.viihdedownloader;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Represents a download in the download list.
 */
public class Download {
    /**
     * The recording being downloaded.
     */
    private final Recording recording;
    /**
     * The path the recording is being downloaded to.
     */
    private final Path path;
    /**
     * Whether or not overwriting the target file is allowed.
     */
    private final boolean allowOverwrite;
    /**
     * The number of bytes in the recording.
     */
    private long totalSize;
    /**
     * The number of bytes currently downloaded.
     */
    private long downloaded;
    /**
     * The number of bytes being downloaded per second.
     */
    private float downloadSpeed;
    /**
     * The state of the download.
     */
    private State state = State.WAITING_METADATA;
    /**
     * The stack trace of the exception that caused this download to fail, if any.
     */
    private String exception;
    /**
     * The job associated with this download.
     */
    private RecordingLoadJob job;

    /**
     * Creates a new download.
     *
     * @param recording      the recording to download
     * @param path           the path to download to
     * @param allowOverwrite whether or not to allow overwriting the target file
     */
    public Download(Recording recording, Path path, boolean allowOverwrite) {
        this.recording = recording;
        this.path = path;
        this.allowOverwrite = allowOverwrite;
    }

    /**
     * Gets the recording being downloaded.
     *
     * @return the recording
     */
    public Recording getRecording() {
        return recording;
    }

    /**
     * Gets the path being downloaded to.
     *
     * @return the path
     */
    public Path getPath() {
        return path;
    }

    /**
     * Gets whether or not overwriting the target file is allowed.
     *
     * @return whether or not overwriting is allowed
     */
    public boolean isAllowOverwrite() {
        return allowOverwrite;
    }

    /**
     * Gets the job associated with this download, or {@code null} if such a job doesn't exist.
     *
     * @return the associated job
     */
    public RecordingLoadJob getJob() {
        return job;
    }

    /**
     * Sets the job associated with this download.
     *
     * @param job the new job
     */
    public void setJob(RecordingLoadJob job) {
        this.job = job;
    }

    /**
     * Gets the state of this download.
     *
     * @return the state
     */
    public State getState() {
        return state;
    }

    /**
     * Gets the stack trace of the exception that caused this job to fail, if any.
     *
     * @return the stack trace
     */
    public String getException() {
        return exception;
    }

    /**
     * Sets the total size of this download and marks the download as ongoing.
     *
     * @param totalSize the size of this download in bytes
     * @see State#DOWNLOADING
     */
    public void setTotalSize(long totalSize) {
        this.totalSize = totalSize;
        state = State.DOWNLOADING;
    }

    /**
     * Sets the currently downloaded size and download speed of this download.
     *
     * @param downloaded    the number of bytes currently downloaded
     * @param downloadSpeed the download speed in bytes per second
     */
    public void setProgress(long downloaded, float downloadSpeed) {
        this.downloaded = downloaded;
        this.downloadSpeed = downloadSpeed;
    }

    /**
     * Marks this download as being restarted.
     *
     * @see State#WAITING_METADATA
     */
    public void restarted() {
        state = State.WAITING_METADATA;
    }

    /**
     * Marks this download as ready to download.
     *
     * @see State#WAITING
     */
    public void metadataLoaded() {
        state = State.WAITING;
    }

    /**
     * Marks this download as complete.
     *
     * @see State#COMPLETE
     */
    public void complete() {
        state = State.COMPLETE;
    }

    /**
     * Marks this download as failed and saves the stack trace of the causing exception.
     *
     * @param throwable the exception that caused the download to fail
     * @see State#FAILED
     */
    public void fail(Throwable throwable) {
        StringWriter sw = new StringWriter();
        Objects.requireNonNull(throwable).printStackTrace(new PrintWriter(sw));
        exception = sw.toString();
        state = State.FAILED;
    }

    /**
     * Marks this download as canceled.
     *
     * @see State#CANCELED
     */
    public void canceled() {
        state = State.CANCELED;
    }

    @Override
    public String toString() {
        switch (state) {
            case WAITING_METADATA:
                return String.format("%s\n%s\nLoading metadata\n%s",
                        recording.getName(),
                        ViihdeDownloader.DATE_FORMAT.format(recording.getDate().getTime()),
                        path.toAbsolutePath().toString());
            case WAITING:
                return String.format("%s\n%s\nWaiting to download\n%s",
                        recording.getName(),
                        ViihdeDownloader.DATE_FORMAT.format(recording.getDate().getTime()),
                        path.toAbsolutePath().toString());
            case DOWNLOADING:
                return String.format("%s\n%s\nDownloading: %.2fMiB of %.2fMiB (%.1f%%) %.2fMiB/s\n%s",
                        recording.getName(),
                        ViihdeDownloader.DATE_FORMAT.format(recording.getDate().getTime()),
                        downloaded / 1048576f,
                        totalSize / 1048576f,
                        100f * downloaded / totalSize,
                        downloadSpeed / 1048576f,
                        path.toAbsolutePath().toString());
            case COMPLETE:
                return String.format("%s\n%s\nDownload complete: %.2fMiB\n%s",
                        recording.getName(),
                        ViihdeDownloader.DATE_FORMAT.format(recording.getDate().getTime()),
                        totalSize / 1048576f,
                        path.toAbsolutePath().toString());
            case FAILED:
                return String.format("%s\n%s\nDownload failed:\n%s\n%s",
                        recording.getName(),
                        ViihdeDownloader.DATE_FORMAT.format(recording.getDate().getTime()),
                        shortenStack(exception),
                        path.toAbsolutePath().toString());
            case CANCELED:
                return String.format("%s\n%s\nDownload canceled\n%s",
                        recording.getName(),
                        ViihdeDownloader.DATE_FORMAT.format(recording.getDate().getTime()),
                        path.toAbsolutePath().toString());
            default:
                throw new AssertionError();
        }
    }

    /**
     * Generates a UI-friendly stack trace by removing all but the first six entries.
     *
     * @param stack the stack trace to shorten
     * @return the shortened stack trace
     */
    private static String shortenStack(String stack) {
        String[] lines = stack.split("\n");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6 && i < lines.length; i++)
            sb.append(i == 0 ? "" : "\n").append(lines[i]);
        if (lines.length > 6)
            sb.append("\n").append(lines.length - 6).append(" more...");
        return sb.toString();
    }

    public enum State {
        /**
         * The download is waiting for the details of the recording to load.
         */
        WAITING_METADATA,
        /**
         * The download is ready to be downloaded.
         */
        WAITING,
        /**
         * The download is ongoing.
         */
        DOWNLOADING,
        /**
         * The download has completed successfully.
         */
        COMPLETE,
        /**
         * The download has failed to complete.
         */
        FAILED,
        /**
         * The download has been canceled.
         */
        CANCELED
    }
}
