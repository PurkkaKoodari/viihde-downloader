package net.pietu1998.viihdedownloader;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ViihdeDownloader extends Application {

    /**
     * The date format to use in the UI.
     */
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("d.M.yyyy H:mm");
    /**
     * The ISO8601 date format used by the API.
     */
    private static final DateFormat ISO8601_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
    /**
     * The root URL of the API.
     */
    private static final String API_URL = "https://api.elisaviihde.fi/etvrecorder/";

    /**
     * The main window.
     */
    private Stage stage;
    /**
     * The search text field.
     */
    private TextField searchField;
    /**
     * The tree view for the folder tree.
     */
    private TreeView<FolderTreeItem> folderTree;
    /**
     * The label for the selection details.
     */
    private Label detailsLabel;
    /**
     * The list of downloads.
     */
    private ListView<Download> downloadList;

    /**
     * The username and password for the user.
     */
    private Pair<String, String> credentials;
    /**
     * Whether or not a login process is ongoing.
     */
    private volatile boolean loggingIn = false;

    /**
     * The root folder.
     */
    private final Folder root = new Folder(null, "", "Recordings");
    /**
     * The map of recording IDs to recordings.
     */
    private final Map<String, Recording> recordingMap = new HashMap<>();
    /**
     * The map of folder IDs to folders.
     */
    private final Map<String, Folder> folderMap = new HashMap<>();
    /**
     * The set of recordings whose details are loading or loaded.
     */
    private final Set<String> loadedRecordings = new HashSet<>();
    /**
     * The map of recording IDs to their corresponding downloads.
     */
    private final Map<String, Download> downloadingRecordings = new HashMap<>();

    /**
     * The job queue for the loader threads.
     */
    private final JobQueue jobQueue = new JobQueue();
    /**
     * The lock for accessing {@link #jobQueue}.
     */
    private final ReentrantLock jobLock = new ReentrantLock();
    /**
     * The condition that is waited on when no jobs are available.
     */
    private final Condition noJobs = jobLock.newCondition();

    /**
     * The shared map of cookies used for contacting the API.
     */
    private final ConcurrentMap<String, String> cookies = new ConcurrentHashMap<>();
    /**
     * The loader threads used.
     */
    private final LoaderThread[] loaders = new LoaderThread[4];

    public ViihdeDownloader() {
        for (int i = 0; i < loaders.length; i++)
            loaders[i] = new LoaderThread(i == 0, cookies, jobQueue, jobLock, noJobs);
    }

    @Override
    public void start(Stage stage) {
        for (LoaderThread loader : loaders)
            loader.start();

        this.stage = stage;
        showLoginDialog(null, true);
    }

    /**
     * Creates the elements of the main GUI. Called after initial login.
     */
    private void buildStage() {
        stage.setTitle("Viihde Downloader");
        stage.setOnCloseRequest(event -> {
            event.consume();
            exit();
        });
        // Search text field
        searchField = new TextField();
        HBox.setHgrow(searchField, Priority.ALWAYS);
        searchField.textProperty().addListener((observable, oldValue, newValue) -> updateSearch(oldValue, newValue));
        // Label for search text field
        Label searchLabel = new Label("Search:");
        searchLabel.setLabelFor(searchField);
        searchLabel.setPadding(new Insets(5));
        // Folder tree view
        folderMap.put("", root);
        TreeItem<FolderTreeItem> rootFolder = new TreeItem<>(root);
        rootFolder.setExpanded(true);
        folderTree = new TreeView<>(rootFolder);
        folderTree.getSelectionModel().getSelectedItems().addListener((InvalidationListener) observable -> updateDetails());
        folderTree.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        folderTree.setCellFactory(treeView -> createTreeCell());
        VBox.setVgrow(folderTree, Priority.ALWAYS);
        // Details text area
        detailsLabel = new Label();
        detailsLabel.setWrapText(true);
        detailsLabel.setAlignment(Pos.TOP_LEFT);
        detailsLabel.setTextAlignment(TextAlignment.LEFT);
        detailsLabel.setMaxHeight(Double.MAX_VALUE);
        detailsLabel.setPadding(new Insets(5));
        VBox.setVgrow(detailsLabel, Priority.ALWAYS);
        // Download button
        Button downloadButton = new Button("Download");
        downloadButton.setOnAction(event -> downloadSelected());
        downloadButton.setMaxWidth(Double.MAX_VALUE);
        // Download list
        downloadList = new ListView<>();
        downloadList.setCellFactory(listView -> createDownloadCell());
        // Assemble search text field and label
        HBox searchBox = new HBox(searchLabel, searchField);
        searchBox.setAlignment(Pos.CENTER);
        // Assemble search and tree
        VBox treeBox = new VBox(searchBox, folderTree);
        // Assemble details and download button
        VBox detailsArea = new VBox(detailsLabel, downloadButton);
        // Assemble GUI
        SplitPane split = new SplitPane(treeBox, detailsArea, downloadList);
        split.setPrefSize(800, 600);
        split.setDividerPositions(0.33, 0.67);
        // Set the GUI to the window
        stage.setScene(new Scene(split));
        stage.setResizable(true);
        stage.show();
        // Start loading the folders
        loadFolder("", "Root folder", rootFolder);
    }

    /**
     * Creates a new {@code TreeCell} with the folder tree double-click listener.
     *
     * @return the new tree cell
     */
    private TreeCell<FolderTreeItem> createTreeCell() {
        TreeCell<FolderTreeItem> cell = new TreeCell<FolderTreeItem>() {
            @Override
            protected void updateItem(FolderTreeItem item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null ? "" : item.toString());
            }
        };
        cell.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2)
                downloadSelected();
        });
        return cell;
    }

    /**
     * Creates a new {@code ListCell} with the download list double-click listener.
     *
     * @return the new list cell
     */
    private ListCell<Download> createDownloadCell() {
        ListCell<Download> cell = new ListCell<Download>() {
            @Override
            protected void updateItem(Download item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null ? "" : item.toString());
            }
        };
        cell.setOnMouseClicked(event -> {
            Download item = cell.getItem();
            if (event.getClickCount() == 2 && item != null) {
                switch (item.getState()) {
                    case CANCELED:
                        break;
                    case WAITING_METADATA:
                    case WAITING:
                    case DOWNLOADING:
                        // Prompt to cancel the download if it's going to be completed in the future.
                        Alert confirm = new Alert(Alert.AlertType.CONFIRMATION, "Cancel the download " +
                                item.getRecording().getName() + "?", ButtonType.YES, ButtonType.NO);
                        confirm.setTitle("Cancel download?");
                        confirm.setHeaderText("Cancel download?");
                        if (confirm.showAndWait().orElse(ButtonType.NO) == ButtonType.YES) {
                            RecordingLoadJob job = item.getJob();
                            if (job == null)
                                // If the job doesn't exist, cancel it from the map.
                                downloadingRecordings.remove(item.getRecording().getId());
                            else if (jobQueue.remove(job))
                                // If the job is not being processed, just unqueue it.
                                job.canceled();
                            else
                                // Otherwise, ask the executing thread to cancel.
                                job.requestCancel();
                        }
                        break;
                    case FAILED:
                        // Show the complete stack trace of the error if the download failed.
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Error details");
                        alert.setHeaderText("Error details");
                        Text errorText = new Text(item.getException());
                        errorText.setWrappingWidth(400);
                        ScrollPane errorScroll = new ScrollPane(errorText);
                        errorScroll.setMaxHeight(500);
                        alert.getDialogPane().setContent(errorScroll);
                        alert.showAndWait();
                        break;
                    case COMPLETE:
                        // Try opening the downloaded file if complete.
                        try {
                            ProcessBuilder builder = new ProcessBuilder(item.getPath().toAbsolutePath().toString());
                            builder.start();
                        } catch (IOException e) {
                            showError("Failed to open video " + item.getRecording().getName() + ".\n" + e.toString());
                        }
                        break;
                }
            }
        });
        return cell;
    }

    /**
     * Called when the search field's text changes.
     *
     * @param oldValue   the previous filter string
     * @param newValue   the new filter string
     */
    private void updateSearch(String oldValue, String newValue) {
        // If the new results are a subset of the previous, use the lighter filtering. Otherwise rebuild the tree.
        if (newValue.toLowerCase().contains(oldValue.toLowerCase()))
            filterTree(folderTree.getRoot(), newValue.toLowerCase());
        else
            rebuildTree(folderTree.getRoot(), root, newValue.toLowerCase());
        folderTree.refresh();
        updateDetails();
    }

    /**
     * <p>Recursively removes any nodes from the descendants of the given node that do not match the filter and have no
     * descendants matching the filter. Returns whether or not there were any matching descendants, and the count of
     * matching recordings found.</p>
     * <p>This operation is less expensive than {@link #rebuildTree(TreeItem, Folder, String)}, but only works for
     * removing elements.</p>
     *
     * @param node   the node to walk
     * @param filter the filter string
     * @return a pair of (any matching descendants found, number of matching recordings)
     */
    private Pair<Boolean, Integer> filterTree(TreeItem<FolderTreeItem> node, String filter) {
        ListIterator<TreeItem<FolderTreeItem>> iterator = node.getChildren().listIterator();
        boolean seen = false;
        int count = 0, ownCount = 0;
        while (iterator.hasNext()) {
            TreeItem<FolderTreeItem> child = iterator.next();
            FolderTreeItem item = child.getValue();
            if (item instanceof Folder) {
                Pair<Boolean, Integer> contentMatch = filterTree(child, filter);
                if (contentMatch.getKey()) {
                    seen = true;
                    count += contentMatch.getValue();
                    child.setExpanded(!filter.isEmpty());
                    continue;
                }
            }
            if (filter.isEmpty() || item.getName().toLowerCase().contains(filter)) {
                seen = true;
                if (item instanceof Recording) {
                    count++;
                    ownCount++;
                }
            } else
                iterator.remove();
        }
        ((Folder) node.getValue()).setMatchedRecordingCount(count);
        ((Folder) node.getValue()).setRecordingCount(ownCount);
        return new Pair<>(seen, count);
    }

    /**
     * <p>Recursively updates the tree to contain only nodes matching the filter or having matching descendants.
     * Returns whether or not there were any matching descendants, and the count of matching recordings found.</p>
     * <p>This operation is more expensive than {@link #filterTree(TreeItem, String)}, but can also add missing
     * elements to the tree.</p>
     *
     * @param node   the node to walk
     * @param folder the
     * @param filter the filter string
     * @return a pair of (any matching descendants found, number of matching recordings)
     */
    private Pair<Boolean, Integer> rebuildTree(TreeItem<FolderTreeItem> node, Folder folder, String filter) {
        ListIterator<TreeItem<FolderTreeItem>> iterator = node.getChildren().listIterator();
        boolean seen = false;
        int count = 0, ownCount = 0;
        TreeItem<FolderTreeItem> currentChild = iterator.hasNext() ? iterator.next() : null;
        for (Folder subfolder : folder.getSubfolders()) {
            // See if the currently walked node contains this subfolder
            boolean childMatch = currentChild != null && currentChild.getValue() == subfolder;
            // Create a new node if it does not
            TreeItem<FolderTreeItem> child = childMatch ? currentChild : new TreeItem<>(subfolder);
            // Recurse to the node's descendants and see if any match
            Pair<Boolean, Integer> contentMatch = rebuildTree(child, subfolder, filter);
            if (subfolder.getName().toLowerCase().contains(filter) || contentMatch.getKey()) {
                seen = true;
                count += contentMatch.getValue();
                child.setExpanded(!filter.isEmpty());
                if (!childMatch) {
                    // Insert before the currently walked node if there is one
                    if (currentChild != null) {
                        iterator.previous();
                        iterator.add(child);
                        iterator.next();
                    } else {
                        iterator.add(child);
                    }
                }
            } else if (childMatch)
                // Remove the currently walked node if it contained this subfolder and didn't match
                iterator.remove();
            if (childMatch)
                currentChild = iterator.hasNext() ? iterator.next() : null;
        }
        for (Recording recording : folder.getRecordings()) {
            boolean childMatch = currentChild != null && currentChild.getValue() == recording;
            if (recording.getName().toLowerCase().contains(filter)) {
                seen = true;
                count++;
                ownCount++;
                // Create a new node if the currently walked node does not contain this recording
                if (!childMatch) {
                    TreeItem<FolderTreeItem> newChild = new TreeItem<>(recording);
                    // Insert before the currently walked node if there is one
                    if (currentChild != null) {
                        iterator.previous();
                        iterator.add(newChild);
                        iterator.next();
                    } else {
                        iterator.add(newChild);
                    }
                }
            } else if (childMatch)
                // Remove the currently walked node if it contained this recording and didn't match
                iterator.remove();
            if (childMatch)
                currentChild = iterator.hasNext() ? iterator.next() : null;
        }
        folder.setMatchedRecordingCount(count);
        folder.setRecordingCount(ownCount);
        return new Pair<>(seen, count);
    }

    /**
     * Creates a {@code LoadJob} that gets a JSON object from the JSON API.
     *
     * @param action     the action of the request, i.e. the last part of the URL path
     * @param parameters the query parameters of the request, alternating keys and values
     * @return the created {@code LoadJob}
     */
    private LoadJob<JSONObject> makeJSONJob(String action, String... parameters) {
        return makeJob(url -> new LoadJob<>(url, JSONObject::new), action, parameters);
    }

    /**
     * Creates a {@code LoadJob} to the JSON API.
     *
     * @param maker      a {@code Function} that creates a {@code LoadJob} of the desired kind from the URL.
     * @param action     the action of the request, i.e. the last part of the URL path
     * @param parameters the query parameters of the request, alternating keys and values
     * @param <T>        the desired return type of the {@code LoadJob}
     * @return the created {@code LoadJob}
     */
    private <T> LoadJob<T> makeJob(Function<URL, LoadJob<T>> maker, String action, String... parameters) {
        try {
            StringBuilder sb = new StringBuilder(API_URL);
            sb.append(action);
            for (int i = 0; i < parameters.length - 1; i += 2) {
                sb.append(i == 0 ? '?' : '&').append(URLEncoder.encode(parameters[i], "UTF-8"));
                sb.append('=').append(URLEncoder.encode(parameters[i + 1], "UTF-8"));
            }
            sb.append(parameters.length > 0 ? '&' : '?').append("ajax=true");
            return maker.apply(new URL(sb.toString()));
        } catch (MalformedURLException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Shows the login dialog.
     *
     * @param error   an error message to show in the dialog; may be {@code null}
     * @param initial whether or not this is the first login, i.e. if {@link #buildStage()} should be called on success
     */
    private void showLoginDialog(String error, boolean initial) {
        // Move to the UI thread if necessary
        if (!Platform.isFxApplicationThread()) {
            Platform.runLater(() -> showLoginDialog(error, initial));
            return;
        }
        // Make the dialog
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Log in");
        dialog.setHeaderText("Log in");
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        // Create the login fields
        Label usernameLabel = new Label("Username:");
        Label passwordLabel = new Label("Password:");
        TextField usernameField = new TextField();
        PasswordField passwordField = new PasswordField();
        usernameLabel.setLabelFor(usernameField);
        passwordLabel.setLabelFor(passwordField);
        // Create the error label
        if (error != null) {
            Label errorLabel = new Label(error);
            grid.add(errorLabel, 0, 0, 2, 1);
        }
        // Assemble the login form
        grid.addRow(error == null ? 0 : 1, usernameLabel, usernameField);
        grid.addRow(error == null ? 1 : 2, passwordLabel, passwordField);
        // Complete the dialog
        dialog.getDialogPane().setContent(grid);
        dialog.getDialogPane().getButtonTypes().addAll(new ButtonType("Login", ButtonBar.ButtonData.OK_DONE),
                new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE));
        dialog.setResultConverter(button -> {
            if (button.getButtonData() == ButtonBar.ButtonData.OK_DONE)
                return new Pair<>(usernameField.getText(), passwordField.getText());
            return null;
        });
        // Request credentials
        credentials = dialog.showAndWait().orElse(null);
        if (credentials == null) {
            exit();
            return;
        }
        startLogin(initial);
    }

    /**
     * Starts logging in with the saved credentials.
     *
     * @param initial whether or not this is the first login, i.e. if {@link #buildStage()} should be called on success
     */
    private void startLogin(boolean initial) {
        if (loggingIn)
            return;
        loggingIn = true;
        jobQueue.setPaused(true);
        cookies.clear();
        LoadJob<Boolean> job = makeJob(LoginLoadJob::new, "default.sl", "username", credentials.getKey(), "password", credentials.getValue());
        job.onSuccess(result -> {
            loggingIn = false;
            if (result) {
                if (initial)
                    Platform.runLater(this::buildStage);
                jobQueue.setPaused(false);
            } else {
                showLoginDialog("Error: invalid credentials.", initial);
            }
        });
        job.onError(throwable -> {
            loggingIn = false;
            showLoginDialog("Error: " + throwable.toString(), initial);
        });
        queueJob(job);
    }

    /**
     * Updates the details area.
     */
    private void updateDetails() {
        detailsLabel.setText(makeDetails());
    }

    /**
     * Gets the selected items from the folder tree.
     *
     * @return the selected items
     */
    private List<FolderTreeItem> getSelectedItems() {
        return folderTree.getSelectionModel().getSelectedItems()
                .stream().filter(Objects::nonNull).map(TreeItem::getValue).collect(Collectors.toList());
    }

    /**
     * Generates the text for the details area.
     *
     * @return the details text
     */
    private String makeDetails() {
        List<FolderTreeItem> selected = getSelectedItems();
        // Keep track if any folders and recordings are selected
        boolean hasRecording = false, hasFolder = false;
        int totalCount = 0;
        for (FolderTreeItem item : selected) {
            // Only count recordings once, i.e. skip them if their parents are also selected
            if (Collections.disjoint(selected, item.ancestorSet()))
                totalCount += item.getRecordingCount();
            // If only one item is selected, make the detail string from it
            if (item instanceof Recording) {
                if (selected.size() == 1) {
                    Recording recording = (Recording) item;
                    if (!recording.isLoaded() && shouldLoad(recording))
                        loadRecording(recording.getId(), recording.getName());
                    return "1 recording selected\n1 recording total\n\n" + recording.getName() + "\n"
                            + DATE_FORMAT.format(recording.getDate().getTime()) + "\n"
                            + recording.getDuration() + "\n\n" + recording.getDescription();
                }
                hasRecording = true;
            } else {
                Folder folder = (Folder) item;
                if (selected.size() == 1) {
                    return "1 folder selected\n" + totalCount + " recording" + (totalCount == 1 ? "" : "s")
                            + " total\n\n" + folder.getName();
                }
                hasFolder = true;
            }
        }
        // State the selections
        return selected.size() + " " + (hasFolder || !hasRecording ? "folders" : "")
                + (hasFolder == hasRecording ? " and " : "") + (hasRecording || !hasFolder ? "recordings" : "")
                + " selected\n" + totalCount + " recording" + (totalCount == 1 ? "" : "s") + " total";
    }

    /**
     * Asks any required details from the user and starts downloading the selected recordings.
     */
    private void downloadSelected() {
        List<FolderTreeItem> selected = getSelectedItems();
        if (selected.size() == 1 && selected.get(0) instanceof Recording) {
            // If only one recording is selected, prompt for the exact filename
            Recording recording = (Recording) selected.get(0);
            FileChooser chooser = new FileChooser();
            chooser.setTitle("Download recording");
            chooser.getExtensionFilters().setAll(new FileChooser.ExtensionFilter("MPEG-2 TS files", "*.ts"));
            chooser.setInitialFileName(cleanFilename(recording.getName(), "Recording") + ".ts");
            File chosen = chooser.showSaveDialog(stage);
            if (chosen == null)
                return;
            // Add the file extension
            if (!chosen.getName().endsWith(".ts"))
                chosen = new File(chosen.getParentFile(), chosen.getName() + ".ts");
            // Create the file so its name is reserved
            try {
                chosen.createNewFile();
            } catch (IOException e) {
                showError("Failed to create file " + chosen.getName() + ".\n" + e.toString());
                return;
            }
            downloadRecording(recording, chosen.toPath(), true);
        } else {
            // Verify that we're actually downloading something. This check may not be bulletproof, but it not working
            // is not a disaster (only shows the prompts and does nothing).
            int totalCount = selected.stream().mapToInt(FolderTreeItem::getRecordingCount).sum();
            if (totalCount == 0)
                return;
            // See if the user wants to keep the same folder names as in the cloud
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Keep folders?");
            alert.setHeaderText("Keep folders?");
            alert.setContentText("Downloading " + totalCount + " recordings. Keep folder structure?");
            alert.getButtonTypes().setAll(ButtonType.YES, ButtonType.NO);
            boolean keepFolders = alert.showAndWait().orElse(ButtonType.NO) == ButtonType.YES;
            // Ask for the root directory
            DirectoryChooser chooser = new DirectoryChooser();
            chooser.setTitle("Choose folder for recordings");
            File chosen = chooser.showDialog(stage);
            if (chosen == null)
                return;
            // Get a stream of the recordings
            String filter = searchField.getText().toLowerCase();
            Stream<Recording> recordings = selected.stream()
                    .filter(item -> Collections.disjoint(selected, item.ancestorSet()))
                    .map(FolderTreeItem::recordings).reduce(Stream.empty(), Stream::concat)
                    .filter(recording -> filter.isEmpty() || recording.getName().toLowerCase().contains(filter));
            // Keep track of the files we're going to create
            Set<String> newFiles = new HashSet<>();
            Path chosenPath = chosen.toPath();
            recordings.forEach(recording -> {
                Path folder = chosenPath;
                if (keepFolders) {
                    // If requested to keep the folder structure, create the folders
                    for (Folder ancestor : recording.getAncestors()) {
                        // Skip the root folder
                        if (ancestor.getParent() == null)
                            continue;
                        // Turn the folder name into a valid filename
                        String ancestorName = cleanFilename(ancestor.getName(), "Folder");
                        Path path = folder.resolve(ancestorName);
                        String name = path.toString().toLowerCase();
                        // Create the folder if it doesn't exist
                        if (!Files.isDirectory(path)) {
                            // Append increasing numbers until the folder names don't clash
                            for (int i = 2; (Files.exists(path) && !Files.isDirectory(path)) || newFiles.contains(name); i++) {
                                path = folder.resolve(ancestorName + " (" + i + ")");
                                name = path.toString().toLowerCase();
                            }
                            try {
                                Files.createDirectory(path);
                            } catch (IOException e) {
                                showError("Failed to create directory " + ancestor.getName() + ".\n" + e.toString());
                                return;
                            }
                        }
                        folder = path;
                    }
                }
                // Turn the recording name to a valid filename
                String filename = cleanFilename(recording.getName(), "Recording");
                Path path = folder.resolve(filename + ".ts");
                String name;
                // Append increasing numbers until the file names don't clash
                for (int i = 2; Files.exists(path) || newFiles.contains(name = path.toString().toLowerCase()); i++)
                    path = folder.resolve(filename + " (" + i + ").ts");
                newFiles.add(name);
                downloadRecording(recording, path, false);
            });
        }
    }

    /**
     * <p>Turns a recording or folder name to a filename that should be allowed in most cases.</p>
     * <p>This removes disallowed characters for Windows ({@code <>:"/\|?*} and control characters) and changes any
     * reserved file names.</p>
     *
     * @param filename    the recording name to turn into a filename
     * @param defaultName the name to use if nothing valid is left of the original
     * @return the filtered filename
     */
    private static String cleanFilename(String filename, String defaultName) {
        filename = filename.trim();
        filename = filename.replaceAll("^(CON|PRN|AUX|NUL|COM\\d|LPT\\d)$|^\\.+|[<>:\"/\\\\|?*\\x00-\\x1f]", "");
        filename = filename.trim();
        if (filename.isEmpty())
            filename = defaultName;
        return filename;
    }

    /**
     * Starts downloading a recording.
     *
     * @param recording      the recording to download
     * @param path           the path to download the recording to
     * @param allowOverwrite whether or not overwriting an existing file is allowed
     */
    private void downloadRecording(Recording recording, Path path, boolean allowOverwrite) {
        // Create the download object
        Download download = downloadingRecordings.computeIfAbsent(recording.getId(), id -> {
            Download load = new Download(recording, path, allowOverwrite);
            downloadList.getItems().add(load);
            return load;
        });
        // Load the details (download URL) for the recording if not loaded
        if (!recording.isLoaded() && shouldLoad(recording)) {
            loadRecording(recording.getId(), recording.getName());
            return;
        }
        download.metadataLoaded();
        downloadList.refresh();
        try {
            RecordingLoadJob job = new RecordingLoadJob(new URL(recording.getDownloadURL()), path, allowOverwrite);
            // Update the download list item when the load job updates
            job.onSuccess(result -> {
                download.complete();
                downloadingRecordings.remove(recording.getId());
                downloadList.refresh();
            });
            job.onError(throwable -> {
                download.fail(throwable);
                downloadList.refresh();
            });
            job.onLengthUpdate(length -> {
                download.setTotalSize(length);
                downloadList.refresh();
            });
            job.onProgress((loaded, speed) -> {
                download.setProgress(loaded, speed);
                downloadList.refresh();
            });
            job.onCancel(() -> {
                download.canceled();
                downloadingRecordings.remove(recording.getId());
                downloadList.refresh();
            });
            job.onRestart(() -> {
                download.setJob(null);
                startLogin(false);
                recording.resetDetails();
                loadRecording(recording.getId(), recording.getName());
                download.restarted();
                downloadList.refresh();
            });
            download.setJob(job);
            queueJob(job);
        } catch (MalformedURLException e) {
            downloadingRecordings.remove(recording.getId());
            showError("Failed to download recording " + recording.getName() + ".");
        }
    }

    /**
     * Starts loading a folder listing.
     *
     * @param id     the ID of the folder
     * @param name   the name of the folder, for error messages
     * @param parent the tree item of the parent folder
     */
    private void loadFolder(String id, String name, TreeItem<FolderTreeItem> parent) {
        LoadJob<JSONObject> job = makeJSONJob("ready.sl", "folderid", id);
        job.onSuccess(result -> Platform.runLater(() -> folderLoaded(id, name, parent, result)));
        job.onError(throwable -> showError("Failed to load folder " + name + ".\n" + throwable.toString()));
        queueJob(job);
    }

    /**
     * Called when a folder listing finishes loading.
     *
     * @param id     the ID of the folder
     * @param name   the name of the folder, for error messages
     * @param parent the tree item of the parent folder
     * @param result the JSON result of the request
     */
    private void folderLoaded(String id, String name, TreeItem<FolderTreeItem> parent, JSONObject result) {
        try {
            Folder current = folderMap.get(id);
            // Convert JSON data to tree items
            JSONObject data = result.getJSONArray("ready_data").getJSONObject(0);
            JSONArray folders = data.getJSONArray("folders");
            for (Object obj : folders) {
                if (!(obj instanceof JSONObject))
                    continue;
                JSONObject jsonObj = (JSONObject) obj;
                String folderId = jsonObj.getString("id");
                String folderName = URLDecoder.decode(jsonObj.getString("name"), "UTF-8");
                Folder folder = new Folder(current, folderId, folderName);
                folderMap.put(folderId, folder);
                TreeItem<FolderTreeItem> item = new TreeItem<>(folder);
                parent.getChildren().add(item);
                loadFolder(folderId, folderName, item);
            }
            JSONArray recordings = data.getJSONArray("recordings");
            for (Object obj : recordings) {
                if (!(obj instanceof JSONObject))
                    continue;
                JSONObject jsonObj = (JSONObject) obj;
                String programId = jsonObj.getString("program_id");
                String programName = URLDecoder.decode(jsonObj.getString("name"), "UTF-8");
                Calendar date = Calendar.getInstance();
                date.setTime(ISO8601_FORMAT.parse(jsonObj.getString("timestamp")));
                Recording recording = new Recording(current, programId, programName, date);
                recordingMap.put(programId, recording);
                parent.getChildren().add(new TreeItem<>(recording));
            }
            // Filter the tree again and update the counts
            filterTree(folderTree.getRoot(), searchField.getText().toLowerCase());
            folderTree.refresh();
        } catch (JSONException | ParseException | NumberFormatException | UnsupportedEncodingException e) {
            showError("Failed to load folder " + name + ".\n" + e.toString());
        }
    }

    /**
     * Checks whether or not a recording's details should start loading, i.e. if they are not already loading or loaded.
     *
     * @param recording the recording to check
     * @return whether or not to load the details
     */
    private boolean shouldLoad(Recording recording) {
        return !loadedRecordings.contains(recording.getId());
    }

    /**
     * Starts loading a recording's details.
     *
     * @param id   the ID of the recording
     * @param name the name of the recording, for error messages
     */
    private void loadRecording(String id, String name) {
        loadedRecordings.add(id);
        LoadJob<JSONObject> job = makeJSONJob("program.sl", "programid", id);
        job.onSuccess(result -> Platform.runLater(() -> recordingLoaded(id, name, result)));
        job.onError(throwable -> recordingLoadFailed(id, name, throwable));
        queueJob(job);
    }

    /**
     * Called when a recording's details finish loading.
     *
     * @param id     the ID of the recording
     * @param name   the name of the recording, for error messages
     * @param result the JSON details
     */
    private void recordingLoaded(String id, String name, JSONObject result) {
        if (!result.has("url")) {
            startLogin(false);
            loadRecording(id, name);
            return;
        }
        try {
            String description = URLDecoder.decode(result.getString("short_text"), "UTF-8");
            String duration = result.getString("flength");
            String url = result.getString("url");
            Recording current = recordingMap.get(id);
            boolean wasLoaded = current.isLoaded();
            current.setDetails(description, duration, url);
            // Start downloading if we just got the URL for it
            Download download = downloadingRecordings.get(id);
            if (!wasLoaded && download != null)
                downloadRecording(current, download.getPath(), download.isAllowOverwrite());
            updateDetails();
        } catch (JSONException | UnsupportedEncodingException e) {
            recordingLoadFailed(id, name, e);
        }
    }

    /**
     * Called when a recording fails to load.
     *
     * @param id    the ID of the recording
     * @param name  the name of the recording, for error messages
     * @param cause the error that caused the loading to fail
     */
    private void recordingLoadFailed(String id, String name, Throwable cause) {
        // Cancel any download
        Download download = downloadingRecordings.remove(id);
        if (download != null)
            download.fail(cause);
        else
            showError("Failed to load recording " + name + ".\n" + cause.toString());
        // Mark the recording's details as no longer loading
        loadedRecordings.remove(id);
    }

    /**
     * Shows an error dialog.
     *
     * @param error the error message
     */
    private void showError(String error) {
        if (!Platform.isFxApplicationThread()) {
            Platform.runLater(() -> showError(error));
            return;
        }
        Alert alert = new Alert(Alert.AlertType.ERROR);
        Text errorText = new Text(error);
        errorText.setWrappingWidth(400);
        alert.getDialogPane().setContent(errorText);
        alert.show();
    }

    /**
     * Queues a {@code LoadJob} for the loader threads.
     *
     * @param job the job to queue
     */
    private void queueJob(LoadJob<?> job) {
        jobLock.lock();
        jobQueue.add(job);
        noJobs.signal();
        jobLock.unlock();
    }

    /**
     * Stops the loader threads and exits the application.
     */
    private void exit() {
        stopLoaders();
        Platform.exit();
    }

    /**
     * Tells the loader threads to stop and waits for them for up to 1 second each.
     */
    private void stopLoaders() {
        for (LoaderThread loader : loaders)
            loader.quit();
        jobLock.lock();
        noJobs.signalAll();
        jobLock.unlock();
        for (LoaderThread loader : loaders) {
            try {
                loader.join(1000);
            } catch (InterruptedException ignore) {}
        }
    }

    public static void main(String[] args) {
        Platform.setImplicitExit(false);
        Application.launch(args);
    }
}
